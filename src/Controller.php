<?php

namespace SJRoyd\Slim\Annotations;

abstract class Controller {

    protected $request;
    protected $response;

    protected $data;
    protected $statusCode = 200;
    protected $jsonEncoding = 0;
    protected $errors = [];

    public function __construct($request, $response) {
        $this->request = $request;
        $this->response = $response;
    }

    public function __get($name)
    {
        return Router::getSlim()->getContainer()->get($name);
    }

    public function beforeExecuteRoute(RouteInfo $info){}

    public function afterExecuteRoute(RouteInfo $info){
        $handler = ResponseHandler::instance();
        $contentType = $handler->getContentType($this->request);
        switch ($contentType) {
            case 'application/json':
                $output = $this->jsonResponse();
                break;
            case 'text/xml':
            case 'application/xml':
                $output = $this->xmlResponse();
                break;
            case 'text/html':
                $output = $this->htmlResponse();
                break;
        }
        return $output;
    }


    protected function response($status, $data){
        $this->statusCode = $status;
        $this->data = $data;
    }

    /**
     * Json response
     * @return string
     */
    private function jsonResponse(){
        $method = $this->request->getMethod();

        switch($method){
            case 'PUT':
                $this->statusCode = 201; break;
            case 'DELETE':
                $this->statusCode = 204; break;
        }

        $response = null;
        switch(floor($this->statusCode/100)){
            case 2:
                $response = $this->data; break;
            case 4:
            case 5:
                $response = [
                    'message' => $this->data
                ];
                if($this->errors){
                    $response['errors'] = $this->errors;
                }
                break;
        }
        return $this->response->withJson($response, $this->statusCode, $this->jsonEncoding);
    }

    /**
     * XML response
     * @return string
     */
    private function xmlResponse(){

    }

    /**
     * HTML response
     * @return string
     */
    private function htmlResponse(){
        $this->response->withStatus($this->statusCode);
        return $this->view->render($this->response, $this->template, $this->data);
    }
}
