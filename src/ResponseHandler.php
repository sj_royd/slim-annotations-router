<?php

namespace SJRoyd\Slim\Annotations;

use Psr\Http\Message\ServerRequestInterface;

class ResponseHandler extends \Slim\Handlers\AbstractHandler {

    /**
     *
     * @var self
     */
    private static $instance;

    /**
     *
     * @return self
     */
    public static function instance(){
        if(!self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Determine which content type we know about is wanted using Accept header
     *
     * @param ServerRequestInterface $request
     * @return string
     */
    public function getContentType(ServerRequestInterface $request){
        return $this->determineContentType($request);
    }
}
